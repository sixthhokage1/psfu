Band Game Controllers
=====================

Controller types:

* Guitar
* Drums
* Keyboard 
* Microphone [TODO: check GH/RB manuals for any special mic/vocal notation symbols]

Rock Band, Guitar Hero, and clones like Frets on Fire have five colored buttons for their instruments:

* Green
* Red
* Yellow
* Blue
* Orange

Rock Band's guitars also have smaller "solo" buttons [TODO: check for solo key notation]

[Need research on other guitar games]