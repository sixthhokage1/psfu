Dancepad Layouts
================

Most dancepads are intended for DDR or other 4-panel games using Up, Down, Left, and Right arrows. These are typically arranged in a 3x3 grid with the center being a "dead" square for rest. On arcade pads the arrows are the only controls, with selection controls being on the cabinet. On pads for home consoles , the corners of the grid are usually used for the ABXY (or equivalent, looking at you PlayStation) buttons with Start and Select being placed on the top edge.

The most common alternative layout for dancepads is the Pump It Up style 5-panel game: Four corners and center. There are few home versions of 5-panel games and thus few pads available.

Thus, to represent dancepad controls, the following buttons will cover them with the exceptions of certain games with unique controls:

* Up Arrow
* Down Arrow
* Left Arrow
* Right Arrow
* Center
* Top Left Arrow
* Top Right Arrow
* Bottom Left Arrow 
* Bottom Right Arrow
* Standard ABXY/equivalent
* Standard Start/Select

Exceptions of note: 

* Dance 86.4 Funky Radio Station: Simplified dance game with three pads aligned in a row, color coded similar to guitar/band game controllers. 
* Dance Aerobics: A game that uses the 3x4 side B of the NES Power Pad. Each button on this side is marked as a colored circle numbered 1 though 12 (numbers progressing left to right, top to bottom). The left two columns are colored blue, the right two columns red.