Typical Buttons/Controls on a Modern Style Gamepad
==================================================

* D-pad or directional buttons
* Left Analog Stick
    * Left Stick Click
* Right Analog Stick
    * Right Stick Click
* A/B/X/Y buttons (PlayStation: X, Square, Circle, Triangle)
* Left Shoulder/Bumper
* Right Shoulder/Bumper
* Left Trigger
* Right Trigger
* Start
* Select
* Home/Guide
* Capture/Share
* Sync/Connect (for wireless controllers)